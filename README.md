![gallery](https://gitlab.com/4ndr3aR/pothole-mix-segmentation/-/raw/main/pics/gallery-3.jpg)

# SHREC 2022 - Pothole Detection - Test Set Videos

Test set videos used in the SHREC '22 track: "Pothole and crack detection on road pavement using RGB-D images"

## The dataset

[Pothole Mix](https://data.mendeley.com/datasets/kfth5g2xk3/2) is a dataset for the semantic segmentation of potholes and cracks on the road surface.

The main dataset is composed of 4340 (image,mask) pairs at different resolutions divided into training/validation/test sets with a proportion of 3340/496/504 images equal to 77/11/12 percent. The repository you're looking at is the test set used in the SHREC '22 competition and it has been used to obtain nice images and videos like this one.

[![video session](https://gitlab.com/4ndr3aR/pothole-mix-segmentation/-/raw/main/pics/VID_20211031_162912.mp4-inference.jpg)](http://deeplearning.ge.imati.cnr.it/genova-5G/video/VID_20211031_162912.mp4-inference.mp4)

This repo is also used as a submodule in the main [SHREC '22 repo](https://gitlab.com/4ndr3aR/pothole-mix-segmentation).
